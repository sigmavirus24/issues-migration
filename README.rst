Installation
------------

1. (Suggested) Create a `virtual environment`_

2. Install the dependencies::

    $ pip install -r requirements.txt


Usage
-----

1. With your `virtual environment`_ active, run::

    $ ./migrate.py github_username/repository gitlab_username/repository

   The first time you run this, you will be prompted for your GitLab and 
   GitHub credentials. After that, you will not be prompted again.

2. Relax.

3. Repeat for every repository whose issues you would like to migrate

Credit
------

- `Cea Stapleton`_

- `Ian Cordasco`_


.. _virtual environment: http://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python/
.. _Cea Stapleton: http://ceastapleton.com
.. _Ian Cordasco: https://twitter.com/sigmavirus24
